@extends('layouts.activeapp')

@section('no-footer')
    
@endsection

@section('content')
    <div class="bg-white">
        <div class="mx-auto max-w-7xl px-6 lg:px-8">
            <div
                class="mx-auto mt-10  grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 border-t border-b border-primary pt-10 pb-14 sm:mt-16 sm:pt-16 lg:mx-0 lg:max-w-none lg:grid-cols-3">
                @foreach ($userPosts as $post)
                    <article class="flex max-w-xl flex-col items-start justify-between">
                        <div class="flex items-center justify-between w-full mb-4">
                            <div class="flex items-center gap-x-4 text-xs">
                                <time datetime="{{ $post->created_at->format('Y-m-d') }}"
                                    class="text-gray-500">{{ $post->created_at->format('M d, Y') }}</time>
                                <a href="#"
                                    class="relative z-10 rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100">{{ $post->theme }}</a>
                            </div>
                            <div class="flex gap-x-2 items-center">
                                <a href="{{ route('edit.post', $post->id) }}"
                                    class="text-xs px-3 py-1.5 rounded-full bg-gray-50 text-sky-600 font-medium hover:bg-gray-300 hover:text-sky-800">Edit</a>
                                <form action="{{ route('delete.post', $post->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <a href="{{ route('delete.post', $post->id) }}" type="submit" data-confirm-delete="true"
                                        class="text-xs px-3 py-1.5 rounded-full bg-gray-50 text-red-600 font-medium hover:bg-gray-300 hover:text-red-800">Delete</a>
                                </form>
                            </div>
                        </div>
                        <div class="group relative">
                            <h3 class="mt-3 text-lg font-semibold leading-6 text-gray-900 group-hover:text-gray-600">
                                <a href="{{ route('post.details', $post->id) }}">
                                    <span class="absolute inset-0"></span>
                                    {{ $post->title }}
                                </a>
                            </h3>
                            <p class="mt-5 line-clamp-3 text-sm leading-6 text-gray-600">{{ $post->description }}</p>
                        </div>
                        <div class="relative mt-4 flex items-center gap-x-4">
                            @php
                                $avatar = $post->user->user_avatar ?? 'images/default.png';
                            @endphp
                            <img id="profile_avatar" src="{{ asset('storage/' . $post->user->user_avatar ?? 'images/default.png') }}" alt=""
                                class="object-cover h-10 w-10 rounded-full bg-gray-50">
                            <div class="text-sm leading-6">
                                <p class="font-semibold text-gray-900">
                                    <a href="#">
                                        <span class="absolute inset-0"></span>
                                        {{ $post->user->name }}
                                    </a>
                                </p>
                                <p class="text-gray-600">{{ $post->user->role == 1 ? 'Admin' : 'User' }}</p>
                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
            {{ $userPosts->links('vendor.pagination.default') }}
        </div>
    </div>
@endsection
