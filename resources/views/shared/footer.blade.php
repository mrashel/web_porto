<footer
    class="fixed bottom-0 left-0 z-20 w-full bg-white border-t border-gray-200 shadow flex items-center justify-between p-6 dark:bg-gray-800 dark:border-gray-600">
    <span class="text-sm text-gray-500 dark:text-gray-400">© 2024 <a href="/"
            class="hover:underline">PSATIRE™</a>. All Rights Reserved.
    </span>
    <ul class="flex items-center space-x-4 text-sm font-medium text-gray-500 dark:text-gray-400">
        <li>
            <a href="#about" class="hover:underline">About</a>
        </li>
    </ul>
</footer>
