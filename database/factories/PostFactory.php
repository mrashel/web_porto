<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = Post::class;

    public function definition(): array
    {
        return [
            'user_id' => User::inRandomOrder()->first()->id,
            'title' => $this->faker->sentence(),
            'theme' => $this->faker->randomElement(['Marketing', 'Business', 'Personal', 'Meme', 'Tech']),
            'description' => $this->faker->paragraph(),
            'image' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
