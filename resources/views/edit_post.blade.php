@extends('layouts.activeapp')

@section('no-footer')
@endsection

@section('content')
    <section class="bg-white dark:bg-gray-900">
        <div class="py-8 px-4 mx-auto max-w-2xl lg:py-4">
            <h2 class="mb-4 text-xl font-bold text-gray-900 dark:text-white">You're Editing a Post</h2>
            <form action="{{ route('update.post', $post->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="grid gap-4 sm:grid-cols-2 sm:gap-6">
                    <div class="sm:col-span-2">
                        <label for="title"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Title</label>
                        <input type="text" name="title" id="title"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-post-600 focus:border-post-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-post-500 dark:focus:border-post-500"
                            value="{{ old('title', $post->title) }}" required="">
                    </div>

                    <div class="sm:col-span-2">
                        <label for="theme"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Theme</label>
                        <input type="text" name="theme" id="theme"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-post-600 focus:border-post-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-post-500 dark:focus:border-post-500"
                            value="{{ old('theme', $post->theme) }}" required="">
                    </div>

                    <div class="sm:col-span-2">
                        <label for="description"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Description</label>
                        <textarea id="description" name="description" rows="8"
                            class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-post-500 focus:border-post-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-post-500 dark:focus:border-post-500"
                            required>{{ old('description', $post->description) }}</textarea>
                    </div>

                    <div class="sm:col-span-2">
                        <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white" for="image">Upload
                            Image</label>
                        <input
                            class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
                            aria-describedby="image_help" id="image" type="file" name="image"
                            value="{{ old('image', $post->image) }}">
                        <div class="mt-1 text-sm text-gray-500 dark:text-gray-300" id="user_avatar_help">It's optional, no
                            pressure!</div>
                    </div>
                </div>
                <button type="submit"
                    class="inline-flex items-center px-5 py-2.5 mt-4 sm:mt-6 text-sm font-medium text-center text-white bg-post-700 rounded-lg focus:ring-4 focus:ring-post-200 dark:focus:ring-post-900 hover:bg-post-800">
                    Edit Post
                </button>
            </form>
        </div>
    </section>
@endsection
