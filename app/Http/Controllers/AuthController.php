<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;

class AuthController extends Controller
{
    public function registerPage()
    {
        return view('register');
    }

    public function registerPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|string|max:70',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'confirm-password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            Alert::error('Form Error', $validator->errors()->first());
            return redirect()->route('register')->withErrors($validator)->withInput();
        }

        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $data['type'] = 0;

            
        //     $defaultAvatarPath = public_path('images/default.png');
    
            
        //     if (!file_exists($defaultAvatarPath)) {
        //         Alert::error('Error', 'Default avatar file not found.');
        //         return redirect()->back()->withInput();
        //     }
    
            
        //     $defaultAvatarName = 'default_' . Str::random(10) . '.png';
    
            
        //     $avatarPath = 'public/user_avatars/' . $defaultAvatarName;
        //     Storage::put($avatarPath, file_get_contents($defaultAvatarPath));
    
            
        //     $data['user_avatar'] = $avatarPath;
        // }

        $user = User::create($data);

        event(new Registered($user));
        Alert::success('Success', 'Your account has been successfully registered. Please login!');
        return redirect()->route('login');
    }

    public function loginPage()
    {
        return view('login');
    }

    public function loginPost(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        if (auth()->attempt($credentials)) {
            $request->session()->regenerate();
            Alert::success('Success', 'Selamat Datang!');

            switch (auth()->user()->type) {
                case 0:
                    return redirect()->route('home');
                case 1:
                    return redirect()->route('');
                default:
                    Alert::error('Error', 'Login Failed!, User type is not recognized.');
                    return redirect('/login');
            }
        } else {
            $user = User::where('email', $request->email)->first();
            if (!$user) {
                Alert::error('Error', 'Login Failed!, Email not found.');
            } else {
                Alert::error('Error', 'Login Failed!, Incorrect Password.');
            }
            return redirect('/login')->withInput();
        }
    }

    public function homePage()
    {
        return view('mainhome');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}
