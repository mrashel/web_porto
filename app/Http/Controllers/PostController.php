<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class PostController extends Controller
{
    public function index()
    {
        // $posts = Post::where('status', 1)->with('user')->get();
        $posts = Post::with('user')->latest()->fastPaginate(9);
        return view('mainhome', compact('posts'));
    }

    public function createPage()
    {
        return view('create_post');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'theme' => 'required|string|max:255',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $imagePath = null;

        if ($request->hasFile('image')) {
            $imagePath = $request->file('image')->store('post_images', 'public');
        }

        $post = new Post([
            'user_id' => Auth::user()->id,
            'title' => $request->get('title'),
            'theme' => $request->get('theme'),
            'description' => $request->get('description'),
            'image' => $imagePath,
            'status' => 1,   // nanti ganti ke 0 karena masih belum ada admin approval
        ]);

        $post->save();

        Alert::success('Success', 'Post created successfully');

        return redirect()->route('home')->with('success', 'Post created successfully');
    }

    public function postDetailsPage($id)
    {
        $postDetails = Post::with('user')->findOrFail($id);
        // dd($postDetails);
        return view('post_details', compact('postDetails'));
    }

    public function userPostsPage($id)
    {
        $userPosts = Post::with('user')->where('user_id', $id)->latest()->fastPaginate(9);
        $title = 'Delete Post!?';
        $text = 'Are you sure you want to delete this post?';
        confirmDelete($title, $text);
        return view('user_posts', compact('userPosts'));
    }

    public function deletePost($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        Alert::success('Success', 'Post deleted successfully');
        return redirect()->route('home')->with('success', 'Post deleted successfully');
    }

    public function editPost($id)
    {
        $post = Post::findOrFail($id);
        return view('edit_post', compact('post'));
    }

    public function updatePost(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'theme' => 'required|string|max:255',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $post = Post::findOrFail($id);

        $post->title = $request->title;
        $post->theme = $request->theme;
        $post->description = $request->description;

        if ($request->hasFile('image')) {
            if ($post->image) {
                Storage::delete($post->image);
            }
            $imagePath = $request->file('image')->store('post_images', 'public');
            $post->image = $imagePath;
        }

        $post->save();

        Alert::success('Success', 'Post updated successfully');

        return redirect()->route('user.posts', ['id' => $post->user_id])->with('success', 'Post updated successfully');
    }
}
