<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profilePage()
    {
        $users = Auth::user();
        return view('user_profile', compact('users'));
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:70',
            'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
            'user_avatar' => 'image|mimes:jpg,jpeg,png|max:2048',
        ]);

        $user = Auth::user();
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if ($request->hasFile('user_avatar')) {
            $avatarPath = $request->file('user_avatar')->store('user_avatars', 'public');
            if ($user->user_avatar) {
                Storage::delete('public/' . $user->user_avatar);
            }
            $user->user_avatar = $avatarPath;
        }

        $user->save();

        Alert::success('Success', 'Profile updated successfully!');

        return redirect()->route('user.profile')->with('success', 'Profile updated successfully!');
    }
}
