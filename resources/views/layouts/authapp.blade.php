<!DOCTYPE html>
<html lang="en" class="scroll-smooth">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    {{-- TAILWIND & FLOWBITE --}}
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="main-content">
                @yield('content')
            </div>
        </div>
    </div>

    {{-- ALT JS SCRIPT FLOWBITE CDN --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>

    {{-- SweetAlert Include --}}
    @include('sweetalert::alert')
</body>

</html>
