<!DOCTYPE html>
<html lang="en" class="scroll-smooth">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    {{-- TAILWIND & FLOWBITE --}}
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap"
        rel="stylesheet">
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            @include('shared.activeheader')
            <div class="main-content">
                @yield('content')
            </div>
            @if (!View::hasSection('no-footer'))
                @include('shared.footer')
            @endif
        </div>
    </div>

    {{-- ALT JS SCRIPT FLOWBITE CDN --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>

    {{-- SweetAlert Include --}}
    @include('sweetalert::alert')

    {{-- CSRF Token --}}
    <script>
        window.csrfToken = "{{ csrf_token() }}";
    </script>

</body>



</html>
