<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $users = [
            [
                'name' => 'rashel',
                'email' => 'rashel@example.com',
                'type' => '0',
                'password' => bcrypt('12345678'),
                'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ];

        foreach ($users as $key => $user) {
            User::create($user);
        }
    }
}
