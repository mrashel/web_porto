@extends('layouts.activeapp')

@section('no-footer')
@endsection

@section('content')
    <style>
        body {
            font-family: 'Plus Jakarta Sans', sans-serif;
        }
    </style>

    <div class="bg-white w-full flex flex-col gap-5 px-3 md:px-16 lg:px-28 md:flex-row text-[#161931]">
        <aside class="hidden py-4 md:w-1/3 lg:w-1/4 md:block">
            <div class="sticky flex flex-col gap-2 p-4 text-sm border-r border-indigo-100 top-12">

                <h2 class="pl-3 mb-4 text-2xl font-semibold">Settings</h2>

                <a href="#"
                    class="flex items-center px-3 py-2.5 font-bold bg-white  text-indigo-900 border rounded-full">
                    Public Profile
                </a>
            </div>
        </aside>
        <main class="w-full min-h-screen py-1 md:w-2/3 lg:w-3/4">
            <div class="p-2 md:p-4">
                <div class="w-full px-6 pb-8 mt-8 sm:max-w-xl sm:rounded-lg">
                    <h2 class="pl-6 text-2xl font-bold sm:text-xl">Public Profile</h2>

                    <form action="{{ route('update.profile') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="grid max-w-2xl mx-auto mt-8">
                            <div class="flex flex-col items-center space-y-5 sm:flex-row sm:space-y-0">

                                <img class="object-cover w-40 h-40 p-1 rounded-full ring-2 ring-indigo-300 dark:ring-indigo-500"
                                    src="{{ asset('storage/' . $users->user_avatar) }}" alt="">

                                <div class="flex flex-col space-y-5 sm:ml-8">
                                    <div class="flex flex-col space-y-5 sm:ml-8">
                                        <input type="file" name="user_avatar" class="hidden" id="user_avatar">
                                        <label for="user_avatar"
                                            class="py-3.5 px-7 text-base font-medium text-indigo-100 focus:outline-none bg-[#202142] rounded-lg border border-indigo-200 hover:bg-indigo-900 focus:z-10 focus:ring-4 focus:ring-indigo-200 cursor-pointer">Change
                                            picture</label>
                                    </div>
                                </div>
                            </div>

                            <div class="items-center mt-8 sm:mt-14 text-[#202142]">

                                <div class="mb-2 sm:mb-6">
                                    <label for="email"
                                        class="block mb-2 text-sm font-medium text-indigo-900 dark:text-white">Your
                                        email</label>
                                    <input type="email" id="email" name="email"
                                        class="bg-indigo-50 border border-indigo-300 text-indigo-900 text-sm rounded-lg focus:ring-indigo-500 focus:border-indigo-500 block w-full p-2.5 "
                                        value="{{ old('email', Auth::user()->email) }}" required>
                                </div>

                                <div class="mb-2 sm:mb-6">
                                    <label for="name"
                                        class="block mb-2 text-sm font-medium text-indigo-900 dark:text-white">Your
                                        Name</label>
                                    <input type="text" id="name" name="name"
                                        class="bg-indigo-50 border border-indigo-300 text-indigo-900 text-sm rounded-lg focus:ring-indigo-500 focus:border-indigo-500 block w-full p-2.5 "
                                        value="{{ old('name', Auth::user()->name) }}" required>
                                </div>
                                {{-- 
                                <div class="mb-6">
                                    <label for="message"
                                    class="block mb-2 text-sm font-medium text-indigo-900 dark:text-white">Bio</label>
                                <textarea id="message" rows="4"
                                    class="block p-2.5 w-full text-sm text-indigo-900 bg-indigo-50 rounded-lg border border-indigo-300 focus:ring-indigo-500 focus:border-indigo-500 "
                                    placeholder="Write your bio here..."></textarea> 
                                </div> --}}

                                <div class="flex justify-end">
                                    <button type="submit"
                                        class="text-white bg-indigo-700  hover:bg-indigo-800 focus:ring-4 focus:outline-none focus:ring-indigo-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-indigo-600 dark:hover:bg-indigo-700 dark:focus:ring-indigo-800">Save</button>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </main>
    </div>
@endsection
