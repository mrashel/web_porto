<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// REGISTER
Route::get('/register', [AuthController::class, 'registerPage'])->name('register');
Route::post('/register', [AuthController::class, 'registerPost'])->name('register.post');

// LOG IN
Route::get('/login', [AuthController::class, 'loginPage'])->name('login');
Route::post('/login', [AuthController::class, 'loginPost'])->name('login.post');

// LOG OUT
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Route::prefix('/user')->middleware(['auth', 'check-access:0'])->group(function () {
    Route::get('home', [PostController::class, 'index'])->name('home');
    Route::get('create-post', [PostController::class, 'createPage'])->name('create.page');
    Route::post('create-post', [PostController::class, 'store'])->name('create.post');
    Route::get('post-details/{id}', [PostController::class, 'postDetailsPage'])->name('post.details');
    Route::get('user-posts/{id}', [PostController::class, 'userPostsPage'])->name('user.posts');
    Route::delete('delete-post/{id}', [PostController::class, 'deletePost'])->name('delete.post');
    Route::get('edit-post/{id}', [PostController::class, 'editPost'])->name('edit.post');
    Route::post('update-post/{id}', [PostController::class, 'updatePost'])->name('update.post');
    Route::get('profile', [ProfileController::class, 'profilePage'])->name('user.profile');
    Route::post('update-profile', [ProfileController::class, 'updateProfile'])->name('update.profile');
});